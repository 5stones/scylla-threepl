from scylla import app
from scylla import configuration
from .client import ThreePLRestClient
from . import tasks


configuration.setDefaults('ThreePL', {
    #'client_id': None,
    #'client_secret': None,
    'debug': False,
    'verify_ssl': True,
    'version': None,
    'timezone': 'US/Central',
})


class App(app.App):

    def _prepare(self):
        config = configuration.getSection('ThreePL')
        client = ThreePLRestClient(
            config['name'],
            config['url'],
            config['client_id'],
            config['client_secret'],
            config['three_pl_key'],
        )
        self.tasks['Order'] = tasks.ThreePLOrderTask(
            client,
            'Order',
            record_prefix=config.get('name', None),
        )

        self.tasks['Item'] = tasks.ThreePLTask(
            client,
            'Item',
            record_prefix=config.get('name', None),
            customer_id=config.get('customer_id'),
            threepl_timezone=config.get('timezone'),
        )

        self.tasks['StockSummary'] = tasks.ThreePLStockSummaryTask(
            client,
            'ThreePLStockSummary-download',
            ('ThreePL', 'Item'),
        )
