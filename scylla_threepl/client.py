import json
from scylla import configuration
from scylla import RestClient
from oauthlib.oauth2 import BackendApplicationClient
from requests.auth import HTTPBasicAuth
from requests_oauthlib import OAuth2Session
import time

# # #####################
# # REQUEST DEBUGGING
# # #####################
# import requests
# import logging
#
# # Enabling debugging at http.client level (requests->urllib3->http.client)
# # you will see the REQUEST, including HEADERS and DATA, and RESPONSE with HEADERS but without DATA.
# # the only thing missing will be the response.body which is not logged.
# try: # for Python 3
#     from http.client import HTTPConnection
# except ImportError:
#     from httplib import HTTPConnection
# HTTPConnection.debuglevel = 1
#
# logging.basicConfig() # you need to initialize logging, otherwise you will not see anything from requests
# logging.getLogger().setLevel(logging.DEBUG)
# requests_log = logging.getLogger("urllib3")
# requests_log.setLevel(logging.DEBUG)
# requests_log.propagate = True


class ThreePLRestError(RestClient.RestError):
    pass


class ThreePLAuthenticationError(ThreePLRestError):
    pass


class ThreePLRestClient(object):
    _successful_codes = [200, 201, 204]

    RestError = ThreePLRestError

    def __init__(self, name, base_url, client_id, client_secret, three_pl_key):
        self.name = name
        self.base_url = base_url
        self.client_id = client_id
        self.client_secret = client_secret
        self.three_pl_key = three_pl_key
        self.default_headers = {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
        }

        self.refresh_client()

    def check_token(self):
        currentTime = time.time()

        if self.token['expires_at'] - currentTime < 600:
            self.refresh_client()

    def refresh_client(self):
        auth = HTTPBasicAuth(self.client_id, self.client_secret)
        oauth_client = BackendApplicationClient(self.client_id)
        self._client = OAuth2Session(client=oauth_client)

        # fetch token
        token = self._client.fetch_token(
            token_url="{}/AuthServer/api/Token".format(self.base_url),
            auth=auth,
            tpl=self.three_pl_key,
            user_login_id="1",
        )

        self.token = token

    def get(self, path, params=None, retry_on_auth_error=True):
        self.check_token()

        response = self._client.request(
            'GET', "{}/{}".format(self.base_url, path),
            params=params,
            headers=self.default_headers,
        )

        try:
            return self._process_response(response)
        except ThreePLAuthenticationError as e:
            if retry_on_auth_error == True:
                return self.get(path, params=params, retry_on_auth_error=False)
            else:
                raise e

    def post(self, path, body, params=None, retry_on_auth_error=True):
        self.check_token()

        response = self._client.request(
            'POST', "{}/{}".format(self.base_url, path),
            json=body,
            params=params,
            headers=self.default_headers,
        )

        try:
            return self._process_response(response)
        except ThreePLAuthenticationError as e:
            if retry_on_auth_error == True:
                return self.post(path, body, params=params, retry_on_auth_error=False)
            else:
                raise e

    def put(self, path, id, body, params=None, retry_on_auth_error=True):
        self.check_token()

        response = self._client.request(
            'PUT', "{}/{}/{}".format(self.base_url, path, id),
            json=body,
            params=params,
            headers=self.default_headers,
        )

        try:
            return self._process_response(response)
        except ThreePLAuthenticationError as e:
            if retry_on_auth_error == True:
                return self.put(path, id, body, params=params, retry_on_auth_error=False)
            else:
                raise e

    def _process_response(self, response):
        # if we received an authentication error, refresh the client token
        if response.status_code == 401:
            self.refresh_client()
            raise ThreePLAuthenticationError('Authentication Error')
        elif response.status_code not in self._successful_codes:
            raise self.RestError('Failed with code: {} and message: {}'.format(response.status_code, response.text))

        return response.json()
