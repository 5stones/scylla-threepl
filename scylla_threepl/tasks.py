from datetime import datetime
from datetime import timedelta
import math
import logging
import json
import re
import scylla
from scylla import orientdb
from . import records
from .convert import to_timezone


class ThreePLTask(scylla.Task):
    """Downloads records from ThreePL."""

    # adjust time buffer for potential discrepencies in 3pl vs our servers
    timing_buffer = 60 * 2

    api_types = {
        # 'Customer': 'customers',
        'Order': 'orders',
        'Item': 'customers/:customer_id/items',
    }

    _logger = logging.getLogger('scylla-threepl')

    def __init__(self, client, record_type, record_prefix='ThreePL', customer_id=None, threepl_timezone='US/Central'):
        self.record_type = record_type.title()
        self.client = client
        self.classname = '{0}{1}'.format(record_prefix, self.record_type)
        self.api_type = self.api_types[self.record_type]
        self.threepl_timezone = threepl_timezone

        if customer_id != None:
            self.api_type = self.api_type.replace(':customer_id', str(customer_id))

        task_id = '{0}-download'.format(self.classname)
        super(ThreePLTask, self).__init__(task_id)

    def _get_show_query_params(self):
        return {}

    def _get_index_query_params(self, after):
        args = {
            'pgsiz': 100
        }

        if after:
            formatted_after = to_timezone(after, timezone=self.threepl_timezone)
            args['rql'] = "LastModifiedDate=ge={}".format(formatted_after)

        return args

    def _step(self, after, options):
        args = self._get_index_query_params(after)

        self._logger.info(u'Getting %s updated after %s', self.api_type, after)
        self.get_all(args)

    def get_all(self, args={}):
        response = None
        if 'pgnum' not in args or not args['pgnum']:
            args['pgnum'] = 1

        while response is None or args['pgnum'] <= int(math.ceil(response['TotalResults'] / float(args['pgsiz']))):
            response = self.client.get('{0}'.format(self.api_type), args)

            if response is not None:
                self._process_search_response(response)
            else:
                self._logger.warning(u"Skipping request '/%s' with args %s as it returned nothing.", self.api_type, args)

            args['pgnum'] += 1

    def process_one(self, rec_id):
        response = self.client.get('{}/{}'.format(self.api_type, rec_id), self._get_show_query_params())
        self._process_search_response(response, False)

    def _process_search_response(self, response, is_collection=True):
        if is_collection == True:
            for obj in response['ResourceList']:
                self._process_response_record(obj)
        else:
            self._process_response_record(response)

    def _process_response_record(self, obj):
        rec = records.ThreePLRecord.factory(self.client, self.record_type, obj)
        with scylla.ErrorLogging(self.task_id, rec):
            rec.throw_at_orient()
            self._link_children(rec)
            self._logger.info(
                u'Saved %s %s as %s %s',
                self.record_type,
                obj.get(rec.key_field),
                rec.classname,
                rec.rid
            )

    def _link_children(self, rec):
        """Once the record and subrecords are in orientdb, add _parent to children
            and children of children recursively, depth first.
        """
        # pull rids of children
        rids = []
        for subtype in rec._sub_records:
            value = rec.get(subtype)
            if isinstance(value, records.ThreePLRecord):
                # single record
                rids.append(value.rid)
                # recursively link children of child
                self._link_children(value)
            elif value:
                # list of records
                for subrecord in value:
                    rids.append(subrecord.rid)
                    # recursively link children of child
                    self._link_children(subrecord)
        # run the update in orientdb
        q = "UPDATE [{}] SET _parent = {}".format(','.join(rids), rec.rid)
        orientdb.execute(q)


class ThreePLOrderTask(ThreePLTask):
    # regexp for ReferenceNum that indicates a split shipment from an original order
    split_re = re.compile(r'^(.*)-(\d)$')

    def _get_show_query_params(self):
        return {
            'detail': 'All',
            'itemdetail': 'All',
        }

    def _get_index_query_params(self, after):
        args = {
            'pgsiz': 100,
            'detail': 'All',
            'itemdetail': 'All',
        }

        if after:
            formatted_after = to_timezone(after, timezone=self.threepl_timezone)
            # check for LastModifiedDate AND ProcessDate, as some processes don't modify 'LastModifiedDate'
            args['rql'] = "(LastModifiedDate=ge={0},ProcessDate=ge={0})".format(formatted_after)

        return args

    def _link_children(self, rec):
        super(ThreePLOrderTask, self)._link_children(rec)

        if isinstance(rec, records.ThreePLOrder):
            ref_match = self.split_re.search(rec.get('ReferenceNum', ''))
            if ref_match and rec.get('ExternalId'):
                self._link_split(ref_match.group(1), rec)

    @staticmethod
    def _link_split(from_ref, to_rec):
        """Draw a `Split` edge from an original Order and a split shipment."""
        query = (
            "SELECT @rid AS rid "
            "FROM ThreePLOrder "
            "WHERE ExternalId = {external_id} "
            " AND ReferenceNum = {ref} "
            " AND {rid} NOT IN out('Split') "
            "LIMIT 1"
        ).format(
            external_id=json.dumps(to_rec.get('ExternalId')),
            ref=json.dumps(from_ref),
            rid=to_rec.rid)

        for row in orientdb.execute(query):
            orientdb.execute("CREATE EDGE Split FROM {} TO {}".format(
                row['rid'], to_rec.rid
            ))


class ToThreePLTask(scylla.ReflectTask):
    """Checks for updates to records an pushes them into 3PL Central.
    """
    link_field = 'Id'
    api_types = {
        'Order': 'orders',
    }

    _logger = logging.getLogger('scylla-threepl')

    def __init__(self,
            from_class,
            conversion,
            client, to_type,
            where=None,
            with_reflection=None):

        super(ToThreePLTask, self).__init__(
            from_class,
            (client.name, to_type),
            where=where,
            with_reflection=with_reflection)

        self.to_type = to_type
        self.api_type = self.api_types[to_type]
        self.client = client
        self.conversion = conversion

    def _process_response_record(self, obj):
        """Uploads the record to 3PL."""

        # create the record in threepl
        data = self.conversion(obj)

        # allow the object to be skipped if the conversion returned false
        if data == False:
            self._logger.debug("Skipping object as conversion returned 'False'")
            return

        is_update = data.get(self.link_field, False)
        threepl_result = None

        if is_update != False:
            threepl_result = self.client.put(self.api_type, is_update, data)
        else:
            threepl_result = self.client.post(self.api_type, data)

        # save the threepl response to orient and link it
        threepl_rec = records.ThreePLRecord.factory(self.client, self.to_type, threepl_result)

        self._save_response(obj, threepl_rec, is_update, request=data)


class ToThreePLOrderTask(ToThreePLTask):
    link_field = 'OrderId'


class ThreePLStockSummaryTask(scylla.UpdateTask):
    """Pulls down stock summary for all downloaded items
    """

    key_field = 'ItemId'
    record_type = 'StockSummary'
    api_type = 'inventory/stocksummaries'

    _logger = logging.getLogger('scylla-threepl')

    def __init__(self, client, task_id, from_class, where=None, lag_time=-2):
        self.client = client
        self.lag_time = lag_time
        super(ThreePLStockSummaryTask, self).__init__(task_id, from_class, where)

    def _format_where(self, after=None, rec_id=None):
        if rec_id:
            rec_id_str = orientdb.encode(rec_id)
            return "WHERE {} IN {}".format(self.key_field, rec_id_str)
        else:
            # modify after date so that we're only grabbing records
            # that haven't been updated for 2 hours
            now_date = datetime.utcnow()
            now_date = now_date + timedelta(hours=self.lag_time)
            now = now_date.strftime('%Y-%m-%dT%H:%M:%S%zZ')
            return "WHERE (_stock_summary_updated_at <= '{}' OR _stock_summary_updated_at IS NULL OR _stock_summary IS NULL)".format(now)

        return "WHERE _stock_summary IS NULL"

    def _process_response_record(self, obj):
        args = {
            'pgsiz': 100,
            'rql': "ItemId=={}".format(obj.get('ItemId')),
        }

        response = self.client.get('{0}'.format(self.api_type), args)
        self._process_search_response(obj, response)

    def _process_search_response(self, item, response):
        summaries = response['Summaries']

        # create a template record
        record = {
            'ItemId': item.get('ItemId'),
            'Summaries': [],
            '_CustomerId': item.get('_CustomerId'),
            '_item': item.get('@rid'),
            '_total_received': 0,
            '_allocated': 0,
            '_available': 0,
            '_on_hold': 0,
            '_on_hand': 0,
        }

        if len(summaries) > 0:
            record['Summaries'] = summaries

            # create total summaries
            for summary in summaries:
                record['_total_received'] += summary.get('TotalReceived')
                record['_allocated'] += summary.get('Allocated')
                record['_available'] += summary.get('Available')
                record['_on_hold'] += summary.get('OnHold')
                record['_on_hand'] += summary.get('OnHand')

        self._save_record(item, record)

    def _save_record(self, item, obj):
        rec = records.ThreePLRecord.factory(self.client, self.record_type, obj)
        with scylla.ErrorLogging(self.task_id, rec):
            result = rec.throw_at_orient()
            self._link_to_item(item, rec)
            self._logger.info(
                u'Saved %s %s as %s %s',
                self.record_type,
                obj.get(rec.key_field),
                rec.classname,
                rec.rid
            )

    def _link_to_item(self, item, rec):
        # link the stock_summary back to the item, and cache the last date it was updated
        now_date = datetime.utcnow()
        now = now_date.strftime('%Y-%m-%dT%H:%M:%S%zZ')
        q = "UPDATE [{}] SET _stock_summary = {}, _stock_summary_updated_at = '{}'".format(item.get('@rid'), rec.rid, now)
        orientdb.execute(q)
