from datetime import timedelta
from decimal import Decimal
from scylla import records
from scylla import convert
from scylla import configuration
from scylla import orientdb
from .convert import parse_tz_unaware

# recursion depth when initializing records
_process_depth = 0

# cache of records cleared after processing a response
_record_cache = {}


class ThreePLRecord(records.ParsedRecord):
    key_field = 'Id'

    factory_base_classname = 'ThreePL'
    classname = 'ThreePLRecord'

    _sub_records = {}
    _linked_records = {}
    _readonly_datetime_fields = []
    _datetime_fields = [
        'CreateDate', 'ExpirationDate', 'CreationDate',
    ]

    # a key value map of fields you'd like to lift
    # to the top level of the object.
    # Ex: { '_CustomerId': ('ReadOnly', 'CustomerIdentifier', 'Id') }
    _flatten_fields = {}

    def __init__(self, client, classname, obj, readonly_link=False):
        super(ThreePLRecord, self).__init__(client, classname, obj, readonly_link)
        self.client = client
        self._readonly_link = readonly_link
        self.threepl_module = client.name
        self._default_timezone = configuration.getSection('ThreePL').get('timezone')
        if classname:
            self.classname = self.threepl_module + classname

    def _process_fields(self):

        if not (self.key_field in self.data):
            # if the key_field exists in a 'ReadOnly' sub-object then lift it up to the root level
            if 'ReadOnly' in self.data and self.key_field in self.data['ReadOnly']:
                self.data[self.key_field] = self.data['ReadOnly'][self.key_field]

        # process datetimes
        if 'ReadOnly' in self.data:
            for field in self._readonly_datetime_fields:
                if field in self.data['ReadOnly']:
                    self.data['ReadOnly'][field] = parse_tz_unaware(self.data['ReadOnly'][field], timezone=self._default_timezone)

        # lift values to the top level of the object
        for key, value in self._flatten_fields.iteritems():
            self.data[key] = self._get_nested_value(self.data, value);

        super(ThreePLRecord, self)._process_fields()

    def _get_nested_value(self, obj, path):
        key = path[0]
        path = path[1:]

        if key not in obj:
            return None
        elif len(path) > 0:
            return self._get_nested_value(obj[key], path)
        else:
            return obj[key]

class ThreePLPackageContent(ThreePLRecord):
    key_field = 'PackageContentId'


class ThreePLPackage(ThreePLRecord):
    key_field = 'PackageId'
    _sub_records = {
        'PackageContents': 'PackageContent',
    }

class ThreePLCustomerIdentifier(ThreePLRecord):
    key_field = 'Id'


class ThreePLContact(ThreePLRecord):
    key_field = 'ContactId'


class ThreePLOrder(ThreePLRecord):
    key_field = 'OrderId'

    _sub_records = {
        'ShipTo': 'Contact',
        'SoldTo': 'Contact',
        'BillTo': 'Contact',
    }

    _datetime_fields = [
        'EarliestShipDate', 'ShipCancelDate', 'RoutePickupDate',
    ]

    _readonly_datetime_fields = [
        'SsnSentDate', 'CreateDate', 'CreationDate', 'ExpirationDate', 'InvoiceDeliveredDate', 'InvoiceExportedDate',
        'LastModifiedDate', 'LoadOutDoneDate', 'PackDoneDate', 'PickDoneDate', 'PickTicketPrintDate', 'PickupDate', 'ProcessDate',
        'ReallocatedAfterPickTicketDate',
    ]

    _flatten_fields = {
        '_CustomerId': ('ReadOnly', 'CustomerIdentifier', 'Id'),
        '_ProcessDate': ('ReadOnly', 'ProcessDate'),
        '_CreationDate': ('ReadOnly', 'CreationDate'),
        '_PackDoneDate': ('ReadOnly', 'PackDoneDate'),
        '_PickDoneDate': ('ReadOnly', 'PickDoneDate'),
        '_PickTicketPrintDate': ('ReadOnly', 'PickTicketPrintDate'),
    }


class ThreePLCustomer(ThreePLOrder):
    key_field = 'CustomerId'

    _sub_records = {
        'CompanyInfo': 'Contact',
        'PrimaryContact': 'Contact',
    }


class ThreePLItem(ThreePLRecord):
    key_field = 'ItemId'

    _datetime_fields = [
        '_stock_summary_updated_at',
    ]

    _flatten_fields = {
        '_CustomerId': ('ReadOnly', 'CustomerIdentifier', 'Id'),
    }


class ThreePLStockSummary(ThreePLRecord):
    key_field = 'ItemId'
