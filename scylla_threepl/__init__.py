"""Core connection to spree and an App to download updates.

configuration required:
    'ThreePL': {
        'name': "ThreePL",
        'url': 'https://...',
        'token': '',
        'verify_ssl': None,
        'debug': False,
    },
"""

from .client import ThreePLRestClient

from .app import App

from .tasks import ThreePLTask, ThreePLOrderTask, ToThreePLTask, ToThreePLOrderTask

from . import records
