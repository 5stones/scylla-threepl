<a name="2.6.0"></a>
# [2.6.0](https://gitlab.com/5stones/scylla-threepl/compare/v2.5.1...v2.6.0) (2019-04-15)


### Features

* **ToThreePLTask:** Allow an object to be skipped if the conversion determines it should ([0e45f30](https://gitlab.com/5stones/scylla-threepl/commit/0e45f30))



<a name="2.5.1"></a>
## [2.5.1](https://gitlab.com/5stones/scylla-threepl/compare/v2.5.0...v2.5.1) (2019-03-28)


### Bug Fixes

* **ThreePLTask, ThreePLRecord:** Fix timezone and server timing offset issues ([c2d89ab](https://gitlab.com/5stones/scylla-threepl/commit/c2d89ab))



<a name="2.5.0"></a>
# [2.5.0](https://gitlab.com/5stones/scylla-threepl/compare/v2.4.1...v2.5.0) (2019-03-13)


### Features

* **ThreePLOrder:** Add PackDoneDate to flattened fields ([bd3f640](https://gitlab.com/5stones/scylla-threepl/commit/bd3f640))



<a name="2.4.1"></a>
## [2.4.1](https://gitlab.com/5stones/scylla-threepl/compare/v2.4.0...v2.4.1) (2019-03-12)


### Bug Fixes

* **ThreePLTask:** Fix rounding issue causing the last page of results to be missed ([ccb6d93](https://gitlab.com/5stones/scylla-threepl/commit/ccb6d93))



<a name="2.4.0"></a>
# [2.4.0](https://gitlab.com/5stones/scylla-threepl/compare/v2.3.0...v2.4.0) (2019-03-12)


### Features

* **ThreePLStockSummaryTask:** Add customer id and link to item on stock summaries ([288cbc4](https://gitlab.com/5stones/scylla-threepl/commit/288cbc4))



<a name="2.3.0"></a>
# [2.3.0](https://gitlab.com/5stones/scylla-threepl/compare/v2.2.1...v2.3.0) (2019-03-12)


### Features

* **ThreePLRecord:** Add the ability to lift nested fields to the top level for indexing ([f3949c9](https://gitlab.com/5stones/scylla-threepl/commit/f3949c9))



<a name="2.2.1"></a>
## [2.2.1](https://gitlab.com/5stones/scylla-threepl/compare/v2.2.0...v2.2.1) (2019-02-14)


### Bug Fixes

* **ThreePLRestClient, ToThreePLTask:** Fix issue with retries not returning the response if successf ([2403a44](https://gitlab.com/5stones/scylla-threepl/commit/2403a44))



<a name="2.2.0"></a>
# [2.2.0](https://gitlab.com/5stones/scylla-threepl/compare/v2.1.0...v2.2.0) (2019-02-14)


### Features

* **ToThreePLTask:** Add error logging on factory method failure ([afabd82](https://gitlab.com/5stones/scylla-threepl/commit/afabd82))



<a name="2.1.0"></a>
# [2.1.0](https://gitlab.com/5stones/scylla-threepl/compare/v2.0.8...v2.1.0) (2019-02-14)


### Features

* **ThreePLOrderTask, ThreePLTask:** Add query parameters to ThreePLOrderTask to pull down full orde ([9ae30ad](https://gitlab.com/5stones/scylla-threepl/commit/9ae30ad))



<a name="2.0.8"></a>
## [2.0.8](https://gitlab.com/5stones/scylla-threepl/compare/v2.0.7...v2.0.8) (2019-01-29)


### Bug Fixes

* **ThreePLOrderTask:** Pull down orders based on both the LastModifiedDate and ProcessDate as some p ([a8098ea](https://gitlab.com/5stones/scylla-threepl/commit/a8098ea))



<a name="2.0.7"></a>
## [2.0.7](https://gitlab.com/5stones/scylla-threepl/compare/v2.0.6...v2.0.7) (2019-01-18)


### Bug Fixes

* **requirements.txt, setup.py:** Fix issue with locking down requests_oauthlib to an old version ([aefd1c2](https://gitlab.com/5stones/scylla-threepl/commit/aefd1c2))



<a name="2.0.6"></a>
## [2.0.6](https://gitlab.com/5stones/scylla-threepl/compare/v2.0.5...v2.0.6) (2019-01-18)


### Bug Fixes

* **requirements.txt, setup.py:** Fix verion requirement typos ([62affac](https://gitlab.com/5stones/scylla-threepl/commit/62affac))



<a name="2.0.5"></a>
## [2.0.5](https://gitlab.com/5stones/scylla-threepl/compare/v2.0.4...v2.0.5) (2019-01-18)


### Bug Fixes

* **setup.py:** Add specific versions to setup.py ([1b56090](https://gitlab.com/5stones/scylla-threepl/commit/1b56090))



<a name="2.0.4"></a>
## [2.0.4](https://gitlab.com/5stones/scylla-threepl/compare/v2.0.3...v2.0.4) (2019-01-18)


### Bug Fixes

* **requirements.txt:** Lock down versions of oauth libraries ([9dbee0d](https://gitlab.com/5stones/scylla-threepl/commit/9dbee0d))



<a name="2.0.3"></a>
## [2.0.3](https://gitlab.com/5stones/scylla-threepl/compare/v2.0.2...v2.0.3) (2019-01-17)


### Bug Fixes

* **ThreePLTask:** Fix issue where threepl request will return nothing ([e915bec](https://gitlab.com/5stones/scylla-threepl/commit/e915bec))



<a name="2.0.2"></a>
## [2.0.2](https://gitlab.com/5stones/scylla-threepl/compare/v2.0.1...v2.0.2) (2019-01-16)


### Bug Fixes

* **setup.py:** Fix missing dependencies from setup script ([459cf0d](https://gitlab.com/5stones/scylla-threepl/commit/459cf0d))



<a name="2.0.1"></a>
## [2.0.1](https://gitlab.com/5stones/scylla-threepl/compare/v2.0.0...v2.0.1) (2019-01-16)


### Bug Fixes

* **ThreePLTask:** Fix an issue with timezone formatting on record download causing missed records ([0070b7f](https://gitlab.com/5stones/scylla-threepl/commit/0070b7f))



<a name="2.0.0"></a>
# [2.0.0](https://gitlab.com/5stones/scylla-threepl/compare/v1.1.2...v2.0.0) (2019-01-08)


### Features

* **records:** Update record field processing ([339341e](https://gitlab.com/5stones/scylla-threepl/commit/339341e))



<a name="1.1.2"></a>
## [1.1.2](https://gitlab.com/5stones/scylla-threepl/compare/v1.1.1...v1.1.2) (2018-12-11)


### Bug Fixes

* **ThreePLRestClient:** Update unauthorized response code from 403 to 401 ([242fde1](https://gitlab.com/5stones/scylla-threepl/commit/242fde1))



<a name="1.1.1"></a>
## [1.1.1](https://gitlab.com/5stones/scylla-threepl/compare/v1.1.0...v1.1.1) (2018-12-11)


### Bug Fixes

* **ThreePLRestClient:** Attempt to refresh authentication upon a 403 response ([1eeec67](https://gitlab.com/5stones/scylla-threepl/commit/1eeec67))



<a name="1.1.0"></a>
# [1.1.0](https://gitlab.com/5stones/scylla-threepl/compare/v1.0.0...v1.1.0) (2018-11-30)


### Bug Fixes

* **setup.py:** Fix url typo ([228e291](https://gitlab.com/5stones/scylla-threepl/commit/228e291))


### Features

* **ThreePLStockSummaryTask:** Add a task to pull down stock summaries for existing items ([f0fbb8e](https://gitlab.com/5stones/scylla-threepl/commit/f0fbb8e))



<a name="1.0.0"></a>
# [1.0.0](https://gitlab.com/5stones/scylla-threepl/compare/0ac2ee0...v1.0.0) (2018-11-06)


### Features

* Add basic functionality for integrating with 3PL Central and pulling down orders ([0ac2ee0](https://gitlab.com/5stones/scylla-threepl/commit/0ac2ee0))
* **app.py, tasks.py:** Add the ability to process individual records, and add appropriate prefix ([a88368a](https://gitlab.com/5stones/scylla-threepl/commit/a88368a))
* **ThreePLRestClient, ToThreePLTask:** Add the ability to push orders into 3PL Central ([f835b11](https://gitlab.com/5stones/scylla-threepl/commit/f835b11))



